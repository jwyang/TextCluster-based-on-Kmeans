import json

path ='../utils/jieba_cut_rumors2_title.json'

def create_vector():
    with open(path,'r') as f:
        word_set = json.load(f)
    ls = []
    vector = []
    for i in word_set:
        for j in i:
            if j not in ls:
                ls.append(j)
    vector.append(ls)
    for item in word_set:
        v = []
        for index in ls:
            v.append(item.count(index)*1.0)
        vector.append(v)

    with open('../utils/rumors2_vector_title.json','w',encoding='utf-8') as f:
        json.dump(vector, f)



if __name__ == '__main__':
    create_vector()