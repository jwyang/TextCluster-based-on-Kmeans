import numpy as np
import json
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN
import os
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
def transfer():
    path = '../utils/rumors2_vector.json'
    with open(path,'r') as f:
        vector = json.load(f)

    vector = np.array(vector[1:])
    column_sum = [float(len(np.nonzero(vector[:, i])[0])) for i in range(vector.shape[1])]
    column_sum = np.array(column_sum)
    column_sum = vector.shape[0] / column_sum
    idf = np.log(column_sum)
    idf = np.diag(idf)

    for doc_v in vector:
        if doc_v.sum() == 0:
            doc_v = doc_v / 1
        else:
            doc_v = doc_v / (doc_v.sum())
        tfidf = np.dot(vector, idf)
        return tfidf

def K_means_cluster():
    base = '../utils/rumors2_title'
    os.mkdir('../utils/cluster_results_title'+'/'+'Kmeans_cluster_results')
    for n_center in range(2,21):
        # pca = PCA(n_components= 0.95)
        # new_tfidf = pca.fit_transform(transfer())
        new_tfidf = transfer()
        k = KMeans(n_clusters=n_center)
        results = k.fit(new_tfidf)
        label = k.labels_
        ls = []
        os.mkdir('../utils/cluster_results_title'+'/'+'Kmeans_cluster_results'+'/'+'center'+str(n_center))
        for i in range(n_center):
            ls.append([j for j, t in enumerate(label) if t == i])
        for j,item in enumerate(ls):
            for i in item:
                path = base + '/' + 'rumor'+str(i+1) + '.txt'
                f1 = open(path,'r',encoding='utf-8')
                content = f1.read()
                f2 = open('../utils/cluster_results_title'+'/'+'Kmeans_cluster_results'+'/'+'center'+str(n_center)+'/'+'cluster'+ str(j+1)+'.txt','a',encoding='utf-8')
                f2.write(content +'\n\n')
                f1.close()
                f2.close()

def DBSCAN_cluster():
    # base = '../utils/rumors2'
    # os.mkdir('../utils/cluster_results' + '/' + 'DBSCAN_cluster_results')
    new_tfidf = transfer()
    pca = PCA(n_components= 3)
    new_tfidf = pca.fit_transform(transfer())
    res = []
    eps = 5
        # 迭代不同的min_samples值
    min_samples = 2
    dbscan = DBSCAN(eps=eps, min_samples=min_samples)
    # 模型拟合
    dbscan.fit(new_tfidf)
    label = dbscan.labels_
    print(label)
    # # 统计各参数组合下的聚类个数（-1表示异常点）
    # n_clusters = len([i for i in set(dbscan.labels_) if i != -1])
    # # 异常点的个数
    # outliners = np.sum(np.where(dbscan.labels_ == -1, 1, 0))
    # # 统计每个簇的样本个数
    # stats = str(pd.Series([i for i in dbscan.labels_ if i != -1]).value_counts().values)
    # res.append({'eps': eps, 'min_samples': min_samples, 'n_clusters': n_clusters, 'outliners': outliners,
    #             'stats': stats})
    # df = pd.DataFrame(res)
    # print(df)
if __name__ == '__main__':
    pca = PCA(n_components= 3)
    new_tfidf = np.array(pca.fit_transform(transfer()))
    x = new_tfidf[:,0]
    y = new_tfidf[:,1]
    z = new_tfidf[:,2]

    ax = plt.figure().add_subplot(111, projection='3d')
    ax.scatter(x,y,z)
    plt.show()
    # K_means_cluster()

