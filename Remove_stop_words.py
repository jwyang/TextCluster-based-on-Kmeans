import jieba
import os
import json

def read_from_file(file_name):
    with open(file_name,"r") as fp:
        words = fp.read()
    return words

def read_from_file_ut(file_name):
    with open(file_name,'r',encoding='utf-8') as fp:
        words = fp.read()
    return words

def stop_words(stop_word_file):
    words = read_from_file(stop_word_file)
    result = jieba.cut(words)
    new_words = []
    for r in result:
        new_words.append(r)
    return set(new_words)

def del_stop_words(words,stop_words_set):
#   words是已经切词但是没有去除停用词的文档。
#   返回的会是去除停用词后的文档
    result = jieba.cut(words)
    new_words = []
    for r in result:
        if r not in stop_words_set:
            new_words.append(r)
    return new_words

if __name__ == "__main__":
    base = '../utils/rumors2'
    base_title = '../utils/rumors2_title'
    aim = '../utils/jieba_cut_rumors2/rumor'
    stop_words_file = '../utils/stop_words.txt'
    stop_words_set = stop_words(stop_words_file)
    print(stop_words_set)
    i = 1
    ls = []
    for item in os.listdir(base_title):
        words = read_from_file_ut(os.path.join(base_title,item))
        new_words = del_stop_words(words,stop_words_set)
        ls.append(new_words)
        i = i+1
    print(ls)
    # with open('../utils/jieba_cut_rumors2.json','w') as f:
    #     json.dump(ls,f)
    with open('../utils/jieba_cut_rumors2_title.json','w') as f:
        json.dump(ls,f)

