from docx import Document

def Draw_text():
    document = Document('../utils/20200208-武汉疫情谣言收集_To OZH_修改.docx')
    base = '../utils/rumors2'
    i = 1
    flag = 1
    for item in document.paragraphs:
        path = base + '/' + 'rumor' + str(i)+'.txt'
        f = open(path,'a+',encoding='utf-8')
        if flag == 1:
            if i <70:
                f.write('1.{}  '.format(str(i)))
                flag = 0
            else:
                f.write('2.{}  '.format(str(i-69)))
                flag = 0
        f.writelines(item.text + '\n')
        if item.text == '':
            f.close()
            i = i + 1
            flag = 1

def Draw_title():
    document = Document('../utils/20200208-武汉疫情谣言收集_To OZH_修改.docx')
    base = '../utils/rumors2_title'
    i = 1
    flag = 1
    for item in document.paragraphs:
        if item.style.name == 'Heading 2':
            path = base + '/' + 'rumor' + str(i) + '.txt'
            f = open(path, 'a+', encoding='utf-8')
            if flag == 1:
                if i < 70:
                    f.write('1.{}  '.format(str(i)))
                    flag = 0
                else:
                    f.write('2.{}  '.format(str(i - 69)))
                    flag = 0
            f.writelines(item.text + '\n')
            f.close()
            i = i + 1
            flag = 1
if __name__ == '__main__':
    Draw_title()
    # document = Document('../utils/20200208-武汉疫情谣言收集_To OZH_修改.docx')
    # for item in document.paragraphs:
    #     if item.style.name == 'Heading 2':
    #         print(item.text)
